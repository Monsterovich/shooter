
#include "myactor.h"

//
//
// ACTOR
//
//

MyActor::MyActor(float _x, float _y, float _width, float _height, int _flags, string _texture, float _velx, float _vely)
{
	x = _x;
	y = _y;
	width = _width;
	height = _height;
	velx = _velx;
	vely = _vely;
	flags = _flags;
	texture = _texture;
	alpha = 1.0;
	state = STATE_READY;
	y_offset = 0;
	intersects = false;
	target = NULL;
	fxticks = 0;
	fticks = 0;
}

void MyActor::Draw(MyTextures &_textures)
{
	if (flags & PROP_INACTIVE)
		return;
	if (!texture.empty())
		glBindTexture(GL_TEXTURE_2D, _textures[texture]);

	float tx1 = 0, tx2 = 1;
	if ((flags & PROP_ANIMATION) || (flags & PROP_FONT))
	{
		tx1 = (float)frame / total_frames;
		tx2 = (float)(frame + 1) / total_frames;
	}
	
	if (flags & PROP_TEX_REVERSE)
		swap(tx1, tx2);
	
	glColor4f (1.0, 1.0, 1.0, alpha);
	glBegin(GL_QUADS);
	glTexCoord2f(tx1, 1+y_offset); glVertex2f(x*ACTORSCALE, y*ACTORSCALE);
	glTexCoord2f(tx2, 1+y_offset); glVertex2f((x+width)*ACTORSCALE, y*ACTORSCALE);
	glTexCoord2f(tx2, y_offset); glVertex2f((x+width)*ACTORSCALE, (y+height)*ACTORSCALE);
	glTexCoord2f(tx1, y_offset); glVertex2f(x*ACTORSCALE, (y+height)*ACTORSCALE);
	glEnd();
}
void MyActor::Tick(vector<MyActor *> &world)
{
	x += velx;
	y += vely;

	for (int i = 0; i < world.size(); i++)
	{
		MyActor *a = world[i];
		// disable intersection with itself
		if (a == this)
			continue;
		if (flags & PROP_SMOKE)
			return;
		if ((a->flags & PROP_ANIMAL) && (flags & PROP_ANIMAL))
			continue;
		intersects = !(X1() > a->X2() || a->X1() > X2() || Y1() > a->Y2() || a->Y1() >Y2());
		if (intersects)
			target = a;
	}
}

//
// FUNCTIONS
//

Label CreateString(const char *str, float x, float y, float size, float dist)
{
	Label result;
	float x_offset = 0;
	for (int i = 0; i < strlen(str); i++)
	{
		char c = str[i];
		MyActor *character = new MyActor(x+x_offset, y, size, size, PROP_ANIMATION | PROP_NOINTERSECTION, "font");
		character->frame = c-32;
		character->total_frames = 128;
		x_offset += size + dist;
		result.push_back(character);
	}
	return result;
}

void EditString(Label &str, const char *newstr)
{
	char id = 0;
	for (int i = 0; i < str.size(); i++)
	{
		if (id >= strlen(newstr))
			str[i]->frame = 0;
		else
			str[i]->frame = newstr[id++]-32;
	}
}

void ShowString(Label &str, bool set)
{
	for (int i = 0; i < str.size(); i++)
	{
		if (!set)
			str[i]->flags |= PROP_INACTIVE;
		else
			str[i]->flags -= PROP_INACTIVE;
	}
}

void AddString(Label &str, MyActors &list)
{
	for (int i = 0; i < str.size(); i++)
		list.push_back(str[i]);
}

//
// MENUENTRY SUBCLASS
//

MenuEntry::~MenuEntry() {
	for (MyActor *a : str) {
		delete a;
	}
}

void MenuEntry::Tick(vector<MyActor *> &world)
{
	MyActor::Tick(world);
}

void MenuEntry::Draw(MyTextures &_textures)
{
	if (flags & PROP_INACTIVE)
		return;
	(void)_textures;
	glDisable(GL_TEXTURE_2D);
	if (focus_ptr && *focus_ptr)
	{
		MenuEntry *current = *focus_ptr;
		if (current->menu_id == menu_id)
			glColor4f (0.25, 0.25, 1.0, alpha);
		else
			glColor4f(1.0, 0.25, 0.25, alpha);
	}
	else
		glColor4f(1.0, 0.25, 0.25, alpha);
	glBegin(GL_QUADS);
	glVertex2f(x*ACTORSCALE, y*ACTORSCALE);
	glVertex2f((x+width)*ACTORSCALE, y*ACTORSCALE);
	glVertex2f((x+width)*ACTORSCALE, (y+height)*ACTORSCALE);
	glVertex2f(x*ACTORSCALE, (y+height)*ACTORSCALE);
	glEnd();
	glEnable(GL_TEXTURE_2D); // is always enabled by default
	for (int i = 0; i < str.size(); i++)
		str[i]->Draw(_textures);
}

void MenuEntry::SetEnabled(bool value)
{
	flags |= PROP_INACTIVE;//value ? PROP_INACTIVE : ~PROP_INACTIVE;
		for (int i = 0; i < str.size(); i++)
			str[i]->flags |= PROP_INACTIVE;//value ? PROP_INACTIVE : ~PROP_INACTIVE;
}

void MenuEntry::AppendChar(char c)
{
	if (s.size() + 1 >= str.size())
		return;
	s += c;
	EditString(str, s.c_str());
}

void MenuEntry::RemoveLastChar()
{
	if (s.size() == 0)
		return;
	s.resize (s.size () - 1);
	EditString(str, s.c_str());
}

float MenuEntry::GetFloat()
{
	return atof(s.c_str());
}
