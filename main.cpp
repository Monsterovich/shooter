
//
//
// INCLUDES
//
//


#include <iostream>
#include <mutex>
#include <thread>

#define NO_STDIO_REDIRECT

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_mouse.h>

// actor stuff here
#include "myactor.h"

//
//
// TYPES
//
//

#define GS_MENU 0
#define GS_INGAME 1

// sound channels
#define CHAN_HIT 0
#define CHAN_FIRE 1

//
//
// VARIABLES
//
//

int gticks = 0, prevgticks = 0;

const char *programName = "Animal shooter!";

SDL_Window *mainWindow;
SDL_GLContext mainContext;

MyTextures texList;
MyActors actorList;
mutex actorsMutex;

MyActor *scope;
MyActor *gun;
MyActor *laser;

MenuEntry *focus, *value1, *value2;

Label str_score;
Label str_time;
#ifdef DEBUG
Label str_debug;
#endif
int score = 0;

Label label1, label2;

int gamestate = 0;

Mix_Chunk *wave_fire = NULL, *wave_hit = NULL;
Mix_Music *music = NULL;

//
//
// FUNCTIONS
//
//

int Random(int low, int high) { return rand() % (high - low + 1) + low; }

bool SetOpenGLAttributes();
void PrintSDL_GL_Attributes();
void CheckSDLError(int line);
void RunGame();
void LoadTexture(string filename, string texname);
void SetUpOpenGL();
void SetUpEngine();
void Restart();
void SpawnProjectile(MyActor *activtor);

//
//
// SYSTEM
//
//

int GetScreenWidth()
{
	return 800;
}

int GetScreenHeight()
{
	return 600;
}

void Crash()
{
	exit(1);
}

bool InitRenderer()
{
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
	{
		cout << "Failed to init SDL\n";
		return false;
	}

	mainWindow = SDL_CreateWindow(programName, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		GetScreenWidth(), GetScreenHeight(), SDL_WINDOW_OPENGL);

	if (!mainWindow)
	{
		cout << "Unable to create window\n";
		CheckSDLError(__LINE__);
		return false;
	}
	SDL_ShowCursor(false);
	SDL_ShowCursor(SDL_DISABLE);
	SDL_SetWindowGrab(mainWindow, SDL_TRUE);

	mainContext = SDL_GL_CreateContext(mainWindow);
	
	SetOpenGLAttributes();
	SDL_GL_SetSwapInterval(1);

	glewExperimental = GL_TRUE;
	glewInit();
	return true;
}

bool SetOpenGLAttributes()
{
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	return true;
}

void LoadResources()
{
	LoadTexture("background.jpg", "ground");
	LoadTexture("smoke.png", "smoke");
	LoadTexture("scope.png", "scope");
	LoadTexture("gun.png", "gun");
	LoadTexture("border.png", "border");
	LoadTexture("bird.png", "bird");
	LoadTexture("laser.png", "laser");
	LoadTexture("font.png", "font");
	LoadTexture("null.png", "null");
	LoadTexture("rabbit.png", "rabbit");
}

void SetUpOpenGL()
{
	// Clear everything
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT);
	SDL_GL_SwapWindow(mainWindow);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
	glEnable(GL_TEXTURE_2D);

	// enable alpha
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);
}

bool InitAudio()
{
	if( Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 4096 ) == -1 ) 
		return false; 

	// Load our music
	music = Mix_LoadMUS("mus.ogg");
	if (!music)
		return false;
	// Load sounds
	wave_fire = Mix_LoadWAV("fire.wav");
	if (!wave_fire)
		return false;
	wave_hit = Mix_LoadWAV("hit.wav");
	if (!wave_hit)
		return false;

	if ( Mix_PlayMusic( music, -1) < 0)
		return false;

	return true;
}

int main(int argc, char *argv[])
{
	(void)argc;
	(void)argv;
	if (!InitAudio())
	{
		cout << "Failed opening audio.\n";
		return 1;
	}

	// Prepare stuff
	SetUpEngine();
	// Run game loop
	RunGame();

	return 0;
}

void ProcessControls(bool &loop)
{
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		if (event.type == SDL_QUIT)
			loop = false;

		if (event.type == SDL_KEYDOWN)
		{
			if (gamestate == GS_MENU)
			{
				char chr = event.key.keysym.sym;
				if (focus)
				{
					if (chr == SDLK_BACKSPACE)
						focus->RemoveLastChar();
					else
					{
						if ((chr >= '0' && chr <= '9') || chr == '.') // only digits and .
							focus->AppendChar(chr);
					}
				}
			}
			switch (event.key.keysym.sym)
			{
			case SDLK_ESCAPE: 
				if (gamestate == GS_MENU)
				{
					loop = false;
					break;
				}
				Restart(); 
				break;
			default:
				break;
			}
		}
		if (event.type == SDL_MOUSEBUTTONDOWN)
		{
			switch (event.button.button)
			{
				case SDL_BUTTON_LEFT:
					if (gun->state == STATE_READY)
					{
						gun->state = STATE_FIRE;
						if (gamestate == GS_INGAME)
							SpawnProjectile(gun);
					}
					break;
				case SDL_BUTTON_RIGHT:
					break;
				default:
					break;
			}
		}
		scope->x = (((float)event.motion.x+1)/GetScreenWidth());
		scope->y = (((float)GetScreenHeight()-(float)event.motion.y+1)/GetScreenHeight());
		scope->x *= 100;
		scope->y *= 100;
		scope->x -= scope->Width()/2;
		scope->y -= scope->Height()/2;
	}
}

void CheckSDLError(int line = -1)
{
	string error = SDL_GetError();

	if (error != "")
	{
		cout << "SLD Error : " << error << endl;
		if (line != -1)
			cout << "\nLine : " << line << endl;
		SDL_ClearError();
	}
}

void LoadTexture(string filename, string texname)
{
	cout << "Loading texture '" + texname + "'...\n";
	SDL_Surface* surface = IMG_Load(filename.c_str());
	if (!surface)
	{
		cout << "Couldn't open texture '" + filename + "'!\n";
		Crash();
	}

	glGenTextures(1, &texList[texname]);
	glBindTexture(GL_TEXTURE_2D, texList[texname]);

	int Mode = GL_RGB;

	if(surface->format->BytesPerPixel == 4) {
		Mode = GL_RGBA;
	}

	glTexImage2D(GL_TEXTURE_2D, 0, Mode, surface->w, surface->h, 0, Mode, GL_UNSIGNED_BYTE, surface->pixels);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}


void DrawGround()
{
	glColor3f (1.0, 1.0, 1.0);
	glBindTexture(GL_TEXTURE_2D, texList["ground"]);
	glBegin(GL_QUADS);
	glTexCoord2f(0, 1); glVertex2f(0.0f, 0.0f);
	glTexCoord2f(1, 1); glVertex2f(1.0f, 0.0f);
	glTexCoord2f(1, 0); glVertex2f(1.0f, 1.0f);
	glTexCoord2f(0, 0); glVertex2f(0.0f, 1.0f);
	glEnd();
}

void DrawBorder()
{
	glColor3f (1.0, 1.0, 1.0);
	glBindTexture(GL_TEXTURE_2D, texList["border"]);
	glBegin(GL_QUADS);
	glTexCoord2f(0, 1); glVertex2f(0.0f, 0.0f);
	glTexCoord2f(1, 1); glVertex2f(1.0f, 0.0f);
	glTexCoord2f(1, 0); glVertex2f(1.0f, 0.05f);
	glTexCoord2f(0, 0); glVertex2f(0.0f, 0.05f);
	glEnd();
}

//
//
// ENGINE
//
//

#define GAMEDELAY 10
#define MAP_SIZE 100
#define GUN_OFFSET -20

int time_offset = 0;

void Restart()
{
	actorsMutex.lock();
	for (MyActor *actor : actorList) {
		delete actor;
	}
	actorList.clear();
	actorsMutex.unlock();
	score = 0;
	gamestate = GS_MENU;
	focus = NULL;

	SetUpEngine();
}

void SetUpEngine()
{
	scope = new MyActor(MAP_SIZE/2, MAP_SIZE/2, 10, 10, PROP_SCOPE, "scope", 0, 0);
	gun = new MyActor(MAP_SIZE/2+GUN_OFFSET, 0, 50/1.5, 80/1.5, PROP_GUN | PROP_ANIMATION, "gun");
	gun->total_frames = 8;
	gun->frame = 0;
	gun->y_offset += 0.05;
	laser = new MyActor(gun->x, gun->y, 5, 50, PROP_LASER, "laser");
	actorList.push_back(laser);
	actorList.push_back(gun);
	str_score = CreateString("SCORE:          ", 5, 95, 5, -2);
	AddString(str_score, actorList);
	str_time = CreateString("TIME:             ", 60, 95, 5, -2);
	AddString(str_time, actorList);
#ifdef DEBUG
	str_debug = CreateString("               ", 5, 90, 5, -2);
	AddString(str_debug, actorList);
#endif

	value1 = new MenuEntry(50, 50, 30, 5, "7.500000", MENU_VALUE1, &focus, true);
	actorList.push_back(value1);
	value2 = new MenuEntry(50, 70, 30, 5, "40.00000", MENU_VALUE2, &focus, true);
	actorList.push_back(value2);
	MenuEntry *play = new MenuEntry(50, 30, 30, 10, "Go!", MENU_BUT_PLAY);
	actorList.push_back(play);
	label1 = CreateString("Animal size: (3-10):", 50, 60, 5, -2.8);
	AddString(label1, actorList);
	label2 = CreateString("Animal speed (10-100):", 50, 80, 5, -2.8);
	AddString(label2, actorList);
	MenuEntry *quit = new MenuEntry(10, 30, 30, 10, "Quit", MENU_BUT_QUIT);
	actorList.push_back(quit);

	actorList.push_back(scope);
}

int spticks = 0;
void SpawnAnimal()
{
	int flags = 0;
	string textures[2] = { "bird", "rabbit" };
	int animal = Random(0, 1);
	if (animal == 1)
		flags |= PROP_TEX_REVERSE;
	string animal_texture = textures[animal];
	float sx, sy;
	float speed = value2->GetFloat();
	if (speed > 100 || speed < 10)
		speed = 40;
	speed /= 100;
	float size = value1->GetFloat();
	if (size > 10 || size < 3)
		size = 7.5;
	sx = size;
	sy = size*2;
	if (animal > 0)
		swap(sx, sy);
	actorList.push_back(new MyActor((float)Random(-1800, -110)/10, (float)Random(40, 80), sx, sy, PROP_ANIMAL | flags, animal_texture, speed, 0));
}

void SpawnSmoke(MyActor *activator)
{
	float velx = cos((float)Random(-314, 314)/100)*(float)Random(0, 255) / 200;
	float vely = sin((float)Random(-314, 314)/100)*(float)Random(0, 255) / 200;
	float size = (float)Random(100, 500)/100;
	MyActor *smoke = new MyActor(activator->x, activator->y, size, size, PROP_SMOKE, "smoke", velx, vely);
	actorList.push_back(smoke);
}

void SpawnProjectile(MyActor *activtor)
{
	Mix_PlayChannel(CHAN_FIRE, wave_fire, 0);
	MyActor *projectile = new MyActor(activtor->x+12, activtor->y+10, 1, 1, PROP_PROJECTILE, "", 0, 8);
	projectile->alpha = 0;
	actorList.push_back(projectile);
}

void ProcessGameplay(bool &loop)
{
#ifdef DEBUG
	char debug_str[16];
	sprintf(debug_str, "Actors: %lu", actorList.size());
	EditString(str_debug, debug_str);
#endif
	if(gamestate == GS_MENU)
	{
		time_offset = gticks;
		EditString(str_score, "Welcome");
		EditString(str_time, "player!");
		laser->flags |= PROP_INACTIVE;
		gun->flags |= PROP_INACTIVE;
		for (int i = 0; i < actorList.size(); i++)
		{
			MyActor *a = actorList[i];
			a->Tick(actorList);

			if (a->flags & PROP_MENU)
			{
				if (a->IsIntersects() && (a->target->flags & PROP_SCOPE))
				{
					MenuEntry *entry = (MenuEntry*)a;
					if (gun->state == STATE_FIRE)
					{
						if (entry->menu_id == MENU_BUT_QUIT)
						{
							loop = false;
							return;
						}
						else if (entry->menu_id == MENU_BUT_PLAY)
						{
							gun->state = STATE_READY;
							gamestate = GS_INGAME;
							laser->flags -= PROP_INACTIVE;
							gun->flags -= PROP_INACTIVE;

							ShowString(label1, false);
							ShowString(label2, false);
							EditString(str_score, "SCORE: 0");
							return;
						}
						else if (entry->menu_id >= MENU_VALUE1)
							focus = entry;
						gun->state = STATE_READY;
					}
				}
			}
		}
		return;
	}

	char time_str[16];
	int total = (gticks - time_offset) / 1000;
	int minutes = total / 60;
	int seconds = total % 60;
	sprintf(time_str, "TIME: %dm:%ds", minutes, seconds);
	EditString(str_time, time_str);

	if (spticks + 10000 < gticks)
	{
		int animal_count = Random(7, 8);
		for (int k = 0; k < animal_count; k++)
			SpawnAnimal();
		spticks = gticks;
	}
	for (int i = 0; i < actorList.size(); i++)
	{
		MyActor *a = actorList[i];
		if (a->flags & PROP_INACTIVE)
			continue;
		if (a->flags & PROP_MENU)
		{
			MenuEntry *entry = (MenuEntry*)a;
			entry->SetEnabled(false);
			continue;
		}

		if (a->flags & PROP_PROJECTILE)
		{
			if (a->flags & PROP_INACTIVE) {
				continue;
			}
			if (a->CenterY() > MAP_SIZE*1.25)
			{
				a->flags |= PROP_INACTIVE;
				continue;
			}
		}

		else if (a->flags & PROP_LASER)
		{
			a->x = gun->x+8;
			a->y = gun->y+20;
			if (gun->state != STATE_READY)
			{
				a->x = 0xFFFFFFFF;
				a->y = 0xFFFFFFFF;
			}
		}

		else if (a->flags & PROP_SMOKE)
		{
			if (a->flags & PROP_INACTIVE) {
				continue;
			}
			if (a->alpha <= 0)
			{
				a->flags |= PROP_INACTIVE;
				continue;
			}
			if (a->fxticks + 1 < gticks)
			{
				a->alpha -= 0.01;
				a->fxticks = gticks;
			}
		}
		else if (a->flags & PROP_GUN)
		{
			const float follow_speed = 0.75*1.25;
			if (a->x < scope->x-5.0)
				a->x += follow_speed;
			if (a->x > scope->x-5.0)
				a->x -= follow_speed;

			if (a->state == STATE_FIRE || a->state == STATE_IDLE)
			{
				if (a->fticks + 125 < gticks)
				{
					if (a->frame > 0)
					{
						gun->state = STATE_IDLE;
					}
					a->frame++;
					if (a->frame > a->total_frames)
					{
						a->frame = 0;
						a->state = STATE_READY;
					}
					a->fticks = gticks;
				}
			}
		}
		else if (a->flags & PROP_ANIMAL)
		{
			if (a->flags & PROP_INACTIVE) {
				continue;
			}
			// objects go outside the map
			if (a->CenterX() > MAP_SIZE*1.25)
			{
				a->flags |= PROP_INACTIVE;
				continue;
			}
			if (a->IsIntersects() && (a->target->flags & PROP_PROJECTILE))
			{
				a->flags |= PROP_INACTIVE;
				score++;
				char str[16];
				sprintf(str, "SCORE: %d", score);
				EditString(str_score, str);
				Mix_PlayChannel(CHAN_HIT, wave_hit, 0);
				for (int j = 0; j < 50; j++)
					SpawnSmoke(a);
			}
		}
		a->Tick(actorList);
	}

	MyActors actorsToDelete;
	// free memory from unused actors
	for (int i = 0; i < actorList.size(); i++)
	{
		MyActor *current = actorList[i];
		// save menu actor because we get values from them
		if ((current->flags & PROP_INACTIVE) && !(current->flags & PROP_MENU))
		{
			actorsToDelete.push_back(current);
			actorList.erase(actorList.begin() + i);
		}
	}

	for (MyActor *actor : actorsToDelete) {
		delete actor;
	}
}

void RunGame()
{
	bool loop = true;

	std::thread gameThread([&] {
		while (loop) {
			gticks = SDL_GetTicks();
			ProcessControls(loop);
			if (prevgticks + GAMEDELAY < gticks)
			{
				ProcessGameplay(loop);
				prevgticks = gticks;
			}
		}

		actorsMutex.lock();
		for (MyActor *actor : actorList) {
			delete actor;
		}
		actorList.clear();
		actorsMutex.unlock();
	});

	std::thread renderThread([&] {
		if (!InitRenderer()) {
			loop = false;
			return;
		}

		LoadResources();
		SetUpOpenGL();

		while (loop)
		{
			DrawGround();

			actorsMutex.lock();
			// draw scope cursor after all actors
			MyActor *_scope = nullptr;
			for (int i = 0; i < actorList.size(); i++)
			{
				if (actorList[i]->flags & PROP_SCOPE) {
					_scope = actorList[i];
					continue;
				}
				actorList[i]->Draw(texList);
			}
			DrawBorder();
			if (_scope) {
				_scope->Draw(texList);
			}
			actorsMutex.unlock();

			SDL_GL_SwapWindow(mainWindow);
		}

		SDL_GL_DeleteContext(mainContext);
		SDL_DestroyWindow(mainWindow);
		SDL_Quit();
	});

	renderThread.join();
	gameThread.join();
}
