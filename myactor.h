
//
// INCLUDES
//

// C
#include <string.h>

// C++
#include <map>
#include <vector>
#include <string>

// OpenGL
#define GL3_PROTOTYPES 1
#include <GL/glew.h>

using namespace std;

typedef map<string, GLuint> MyTextures;

//
// TYPES
//

#define STATE_READY 0
#define STATE_FIRE 1
#define STATE_IDLE 2

#define MENU_BUT_QUIT -1
#define MENU_BUT_PLAY 1
#define MENU_VALUE1 2
#define MENU_VALUE2 3

#define PROP_NONE 0
#define PROP_NOINTERSECTION (1<<2)
#define PROP_SMOKE (1<<3)
#define PROP_MISSILE (1<<4)
#define PROP_TEX_REVERSE (1<<5)
#define PROP_INACTIVE (1<<6)
#define PROP_ANIMATION (1<<7)
#define PROP_SCOPE (1<<8)
#define PROP_GUN (1<<9)
#define PROP_ANIMAL (1<<10)
#define PROP_PROJECTILE (1<<11)
#define PROP_LASER (1<<12)
#define PROP_FONT (1<<13)
#define PROP_MENU (1<<14)
#define PROP_INPUT (1<<15)

class MyActor
{
public:
	MyActor(float _x, float _y, float _width, float _height, int _flags = 0, string _texture = "", float _velx = 0, float _vely = 0);
	virtual ~MyActor() {}
	float X1() { return x; }
	float Y1() { return y; }
	float X2() { return x + width; }
	float Y2() { return y + height; }
	float Width() { return width; }
	float Height() { return height; }
	float CenterX() { return x + width / 2; }
	float CenterY() { return y + height / 2; }
	bool IsIntersects() { return intersects; }
	virtual void Draw(MyTextures &_textures);
	virtual void Tick(vector<MyActor *> &world);
public:
	float x, y, alpha, velx, vely, y_offset;
	int fticks, fxticks;
	int flags, state;
	string texture;
	int total_frames, frame;
	MyActor *target;
protected:
	float width, height;
	bool intersects;
};

typedef vector<MyActor*> MyActors;
typedef MyActors Label;

Label CreateString(const char *str, float x, float y, float size, float dist);
void EditString(Label &str, const char *newstr);
void ShowString(Label &str, bool set);
void AddString(Label &str, MyActors &list);

class MenuEntry : public MyActor
{
public:
	MenuEntry(float _x, float _y, float _width, float _height, const char *_str, int _menu_id, MenuEntry **_focus_ptr = NULL, bool input = false, float _alpha = 0.25) :
		MyActor(_x, _y, _width, _height, PROP_MENU, "")
	{
		alpha = _alpha;
		str = CreateString(_str, CenterX()-12, CenterY()-3, 5, -2);
		menu_id = _menu_id;
		s = _str;
		if (input)
			flags |= PROP_INPUT;
		focus_ptr = _focus_ptr;
	}
	virtual ~MenuEntry();

	void Tick(vector<MyActor *> &world);
	void Draw(MyTextures &_textures);
	void SetEnabled(bool value);
	void AppendChar(char c);
	void RemoveLastChar();
	float GetFloat();
	void Clean();
public:
	int menu_id;
private:
	MenuEntry **focus_ptr;
	Label str;
	string s;
};

#define ACTORSCALE 0.01


